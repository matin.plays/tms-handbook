module.exports = ctx => ({
  plugins: [
    [
      "@vuepress/pwa",
      {
        serviceWorker: true,
        updatePopup: true,
      },
    ],
  ],
  head: [
    ['link', { rel: 'icon', href: '/TMS-Logo.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#787878' }]],

  dest: "public/",
  title: "TMS Handbook",
  description: "The unofficial TMS Handbook",

  themeConfig: {
    repo: "https://gitlab.com/pinewood-builders/TMS-Handbook",
    editLinks: true,
    docsDir: "docs/",
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: "green",
      disableThemeIgnore: true,
    },

    algolia: ctx.isProd ? ({
      apiKey: '8d22e1eefdbc0248fed9c76b1d115c9f',
      indexName: 'pinewood-builders_tms'
  }) : null,

    logo: "/TMS-Logo.png",
    smoothScroll: true,

    nav: [
      {
        text: "Home",
        link: "/",
      },
      {
        text: "TMS-Handbook",
        link: "/handbook/",
      },
      {
        text: 'Pinewood',
        items: [{
          text: 'Pinewood Homepage',
          link: 'https://pinewood-builders.com'
        },
          {
            text: 'PBST Handbook',
            link: 'https://pbst.pinewood-builders.com'
          },
          {
            text: 'PET Handbook',
            link: 'https://pet.pinewood-builders.com'
          }
        ]
      },
    ],

    sidebarDepth: 2,
  }
}
)
